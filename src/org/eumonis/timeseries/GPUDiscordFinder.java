
package org.eumonis.timeseries;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.amd.aparapi.Kernel;

/**
 * Calculate discord of a time series.
 *  
 * @author sdienst
 */

public class GPUDiscordFinder{

   public static void main(String[] _args) throws NumberFormatException, IOException {

	  List<Float> series = new ArrayList<Float>();
	  BufferedReader br=new BufferedReader(new FileReader(new File("d:/temp/series")));
	  String line = null;
	  while((line=br.readLine())!=null)
		  series.add(Float.parseFloat(line));
	  

	  final int size = series.size();
      final float[] values = new float[size];
      for (int i = 0; i < size; i++) {
    	  values[i]=series.get(i);
      }
      System.out.println(size);
      final float[] subseqsums = new float[size];
      final float[] bestDist = new float[1];
      final int[] bestLoc = new int[1];
      final int window = 600;
      bestDist[0]=0;
      bestLoc[0]=-1;
      Kernel kernel = new Kernel(){
    	  // if a kernel runs for >2sec, windows kills the driver to keep the desktop responsive!
    	  float subarrayDistance(int s1, int s2){
    		  float sum = 0;

    		  for(int i = 0; i<window; i++){
    			  float diff = values[s1+i]-values[s2+i];
    			  sum+=diff*diff;
    		  }
    		  return sqrt(sum);
    	  }
         @Override public void run() {
            int gid = getGlobalId();
            if(gid >= size - window  )
            	return;
            float min = 10000000;//nearly infinity...
            for(int i=0; i<size - window ; i+=10){
            	if(abs(i-gid)>window){
            		if(min>bestDist[0])
            		min=min(min,subarrayDistance(gid, i));
            	}
            }
            subseqsums[gid]=min;
            if(min>bestDist[0]){
            	bestDist[0]=min;
            	bestLoc[0]=gid;
            }
         }
      };
      long start=System.currentTimeMillis();
      kernel.execute(size);
      System.out.println(System.currentTimeMillis()-start + "ms");
      System.out.println("Execution mode=" + kernel.getExecutionMode());
      System.out.printf("Best loc: %d, best dist: %f", bestLoc[0], bestDist[0]);
      
      FileWriter fw = new FileWriter("d:/temp/series.out");
      // Display computed square values.
      for (int i = 0; i < size; i++) {
         //System.out.printf("%6.0f %8.0f\n", values[i], subseqsums[i]);
    	  fw.write(""+i+"\t"+values[i]+"\t"+subseqsums[i]+"\n");
      }
      fw.close();
      kernel.dispose();
   }

}
